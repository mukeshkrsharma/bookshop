package com.ait.BookShop.data;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.ait.BookShop.model.Book;

@Stateless
@LocalBean
public class BookDAO {

	@PersistenceContext
	private EntityManager em;

	public List<Book> findAllBooks() {
		Query query = em.createQuery("Select b from Book b");
		return query.getResultList();
	}

	public List<Book> findBooksByTitle(String title) {
		Query query = em.createQuery("SELECT n FROM Book AS n "
				+ "WHERE n.title LIKE ?1");
		query.setParameter(1, "%" + title.toUpperCase() + "%");
		return query.getResultList();
	}

	public List<Book> findBooksByAuthor(String author) {
		Query query = em.createQuery("SELECT n FROM Book AS n "
				+ "WHERE n.author LIKE ?1");
		query.setParameter(1, "%" + author.toUpperCase() + "%");
		return query.getResultList();
	}

	public List<Book> findBooksByPrice(String price) {
		Query query = em.createQuery("SELECT n FROM Book AS n "
				+ "WHERE n.price LIKE ?1");
		query.setParameter(1, "%" + price.toUpperCase() + "%");
		return query.getResultList();
	}

	public Book findBookByBookId(int id) {
		return em.find(Book.class, id);
	}

	public void save(Book Book) {
		// em.persist(Book);
		em.merge(Book);
	}

	public void update(Book Book) {
		System.out
				.println("Book with id: " + Book.getBookId() + " was updated");
		em.merge(Book);
	}

	public void delete(int id) {
		System.out.println("Book with id: " + id + " was deleted.");
		em.remove(findBookByBookId(id));
	}

}