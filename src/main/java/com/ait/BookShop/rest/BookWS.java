package com.ait.BookShop.rest;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.ait.BookShop.data.BookDAO;
import com.ait.BookShop.model.Book;

@Path("/book")
@Stateless
@LocalBean
public class BookWS {

	@EJB
	private BookDAO bookDAO;

	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	public Response getAllBooks() {
		System.out.println("Get all Books");
		List<Book> books = bookDAO.findAllBooks();
		System.out.println("...got Books...");
		System.out.println(books.size());
		return Response.status(200).entity(books).build();
	}

	@GET
	@Path("/title/{query}")
	@Produces({ MediaType.APPLICATION_JSON })
	public Response getByTitle(@PathParam("query") String query) {
		System.out.println("findByTitle: " + query);
		List<Book> books = bookDAO.findBooksByTitle(query);
		return Response.status(200).entity(books).build();
	}

	@GET
	@Path("/author/{query2}")
	@Produces({ MediaType.APPLICATION_JSON })
	public Response getBooksByAuthor(@PathParam("query2") String query) {
		System.out.println("findBooksByAuthor: " + query);
		List<Book> books = bookDAO.findBooksByAuthor(query);
		return Response.status(200).entity(books).build();
	}

	@GET
	@Path("/price/{query3}")
	@Produces({ MediaType.APPLICATION_JSON })
	public Response getBooksByPrice(@PathParam("query3") String query) {
		System.out.println("findBooksByPrice: " + query);
		List<Book> books = bookDAO.findBooksByPrice(query);
		return Response.status(200).entity(books).build();
	}

	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	@Path("/{id}")
	public Response getBookByBookId(@PathParam("id") int id) {
		Book book = bookDAO.findBookByBookId(id);
		return Response.status(200).entity(book).build();
	}

	@POST
	@Produces({ MediaType.APPLICATION_JSON })
	public Response saveBook(Book book) {
		bookDAO.save(book);
		return Response.status(201).entity(book).build();
	}

	@PUT
	@Path("/{id}")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_JSON })
	public Response updateBook(Book book) {
		bookDAO.update(book);
		return Response.status(200).entity(book).build();
	}

	@DELETE
	@Path("/{id}")
	public Response deleteBook(@PathParam("id") int id) {
		bookDAO.delete(id);
		return Response.status(204).build();
	}

}
