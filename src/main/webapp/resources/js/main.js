// JavaScript Document
var rootUrl = "http://localhost:8080/BookShop/rest/book";

var renderList = function(data) {
	console.log("response");

	$
			.each(
					data,
					function(index, book) {

						$('#table_body')
								.append(
										'<tr id = "'
												+ book.bookId
												+ '">'
												+ '<td>'
												+ book.title
												+ '</td><td>'
												+ book.author
												+ '</td><td>'
												+ book.publisher
												+ '</td><td>'
												+ book.edition
												+ '</td><td>'
												+ book.isbn
												+ '</td><td>'
												+ book.price
												+ '</td><td>'
												+ book.description
												+ '</td><td id="'
												+ book.bookId
												+ '"><a id="edit" href="#"data-toggle="modal" data-target="#exampleModal"><span class="fa fa-pencil">Edit</a></td></tr>');
					});
	$('#table_id').DataTable();
	output = '<div class="row">';

	$
			.each(
					data,
					function(index, book) {
						output += ('<div class="col-sm-6 col-md-4 col-lg-3"><div class="card"><img src="resources/images/'
								+ book.picture
								+ '" height="150"><p>Title: '
								+ book.title
								+ '</p><p>Author: '
								+ book.author
								+ '</p><p>ISBN: '
								+ book.isbn
								+ '</p><p>Price: ' + book.price + '</p></div></div>');

					});

	output += '</div>';
	$('#bookList').append(output);
};

var findAll = function() {
	console.log('findAll');
	$.ajax({
		type : 'GET',
		url : rootUrl,
		dataType : "json",
		success : renderList
	});
};

$(document).on("click", "#table_id tr", function() {
	findByBookId(this.id);
});

var currentBook;

var findByBookId = function(bookId) {
	console.log('findByBookId: ' + bookId);
	$.ajax({
		type : 'GET',
		url : rootUrl + '/' + bookId,
		dataType : "json",
		success : function(data) {
			$('#btnDelete').show();
			console.log('findByBookId success: ' + data.title);
			currentBook = data;
			renderDetails(currentBook);
		}

	});
};

var renderDetails = function(book) {
	$('#bookId').val(book.bookId);
	$('#title').val(book.title);
	$('#author').val(book.author);
	$('#publisher').val(book.publisher);
	$('#edition').val(book.edition);
	$('#isbn').val(book.isbn);
	$('#price').val(book.price);
	$('#picture').attr('src', 'resources/images/' + book.picture);
	$('#description').val(book.description);

};

function addBook() {
	console.log('addBook');
	$.ajax({
		type : 'POST',
		contentType : 'application/json',
		url : rootUrl,
		dataType : "json",
		data : formToJSONAdd(),
		success : function(data, textStatus, jqXHR) {
			$("#success").modal('show');
			$('#bookId').val(data.bookId);
			$('#table_body').empty();
			$('#bookList').empty();
			findAll();

		},
		error : function(jqXHR, textStatus, errorThrown) {
			$("#error").modal('show');
		}
	});
};

function deleteBook() {
	console.log('deleteBook');
	$.ajax({
		type : 'DELETE',
		url : rootUrl + '/' + $('#bookId').val(),
		success : function(data, textStatus, jqXHR) {
			$("#success").modal('show');
			$('#table_body').empty();
			$('#bookList').empty();
			findAll();

		},
		error : function(jqXHR, textStatus, errorThrown) {
			$("#error").modal('show');
		}
	});
};

function updateBook() {
	console.log('updateBook');
	$.ajax({
		type : 'PUT',
		contentType : 'application/json',
		url : rootUrl + '/' + $('#bookId').val(),
		dataType : "json",
		data : formToJSON(),
		success : function(data, textStatus, jqXHR) {
			$("#success").modal('show');
			$('#table_body').empty();
			$('#bookList').empty();
			findAll();

		},
		error : function(jqXHR, textStatus, errorThrown) {
			$("#error").modal('show');
		}
	});
};
function formToJSONAdd() {
	var bookId = $('#bookId').val();
	return JSON.stringify({
		"bookId" : bookId == "" ? null : bookId,
		"title" : $('#title').val(),
		"author" : $('#author').val(),
		"publisher" : $('#publisher').val(),
		"edition" : $('#edition').val(),
		"price" : $('#price').val(),
		"isbn" : $('#isbn').val(),
		"picture" : $('#picture').val(),
		"description" : $('#description').val()
	});
}
function formToJSON() {
	var bookId = $('#bookId').val();
	return JSON.stringify({
		"bookId" : bookId == "" ? null : bookId,
		"title" : $('#title').val(),
		"author" : $('#author').val(),
		"publisher" : $('#publisher').val(),
		"edition" : $('#edition').val(),
		"price" : $('#price').val(),
		"isbn" : $('#isbn').val(),
		"picture" : currentBook.picture,
		"description" : $('#description').val()
	});
}
var resetForm = function() {
	$('#bookId').val("");
	$('#title').val("");
	$('#author').val("");
	$('#publisher').val("");
	$('#edition').val("");
	$('#price').val("");
	$('#isbn').val("");
	$('#picture').val("");
	$('#description').val("");

}

$(document).ready(
		function() {
			findAll();

			$("#slideshow > div:gt(0)").hide();

			setInterval(function() {
				$('#slideshow > div:first').fadeOut(1000).next().fadeIn(1000)
						.end().appendTo('#slideshow');
			}, 3000);

			$('#btnAdd').click(function() {
				addBook();
				resetForm();

			});
			$('#btnClear').click(function() {
				resetForm();
			});
			$('#btnSave').click(function() {
				updateBook();
				resetForm();

			});

			$('#btnDelete').click(function() {
				deleteBook();
				resetForm();

			});

		});