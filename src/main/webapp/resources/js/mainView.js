var books;
var booksView;
var booksTableView;
var bookModalView;
var book;
var selectRowData;

var Book = Backbone.Model.extend({
	urlRoot : "http://localhost:8080/BookShop/rest/book"
});
var Books = Backbone.Collection.extend({
	model : Book,
	url : "http://localhost:8080/BookShop/rest/book"
});

var BookView = Backbone.View.extend({
	tagName : "div",
	className : "col-sm-6 col-md-4 col-lg-3 card",
	render : function() {
		var template = _.template($("#bookTemplate").html());
		var html = template(this.model.toJSON());
		this.$el.html(html);
		return this;
	}
});

var BooksView = Backbone.View.extend({
	render : function() {
		this.collection.each(function(book) {
			var bookView = new BookView({
				model : book
			});
			this.$el.append(bookView.render().el);
		}, this);
	}
});

var BooksTableView = Backbone.View
		.extend({
			initialize : function() {
			},
			render : function() {
				books = new Books();
				books
						.fetch({
							success : function(books) {
								$('#table_id')
										.DataTable(
												{
													data : books.toJSON(),
													columns : [
															{
																"data" : "title"
															},
															{
																"data" : "author"
															},
															{
																"data" : "publisher"
															},
															{
																"data" : "edition"
															},
															{
																"data" : "isbn"
															},
															{
																"data" : "price"
															},
															{
																"data" : "description"
															},
															{
																"defaultContent" : "<button type='button' class='btn btn-primary' data-toggle='modal' data-target='#exampleModal'>Edit</button>"
															} ]
												});

							}
						});
			}
		});
var BookModalView = Backbone.View.extend({
	render : function() {
		var template = _.template($("#bookModalTemplate").html());
		var html = template(this.model.toJSON());
		this.$el.html(html);
		return this;
	}
});

// ---------------------------------------------------------DOM
$(document).ready(
		function() {

			$("#viewPage").click(function() {
				$('#bookList').html('');
				books = new Books();
				books.fetch({
					success : function(data) {
						booksView = new BooksView({
							el : "#bookList",
							collection : books
						});
						booksView.render();
					}
				});
			});

			$("#adminPage").click(function() {
				booksTableView = new BooksTableView();
				$('#table_id').DataTable().destroy();
				booksTableView.render();
			});

			$('#table_id').on(
					'click',
					'.btn-primary',
					function() {
						selectRowData = $('#table_id').DataTable().row(
								$(this).parents('tr')).data();
						book = new Book(selectRowData);
						bookModalView = new BookModalView({
							el : "#modalBody",
							model : book
						});
						bookModalView.render();
						$('#btnDelete').prop('disabled', false);
						$('#btnAdd').prop('disabled', false);
						$('#btnSave').prop('disabled', false);
						$('#btnClear').prop('disabled', false);
					});

			$('#btnClear').click(function() {
				book = new Book({
					bookId : "",
					title : "",
					author : "",
					publisher : "",
					edition : "",
					isbn : 0,
					price : 0,
					picture : "",
					description : ""
				});
				bookModalView = new BookModalView({
					el : "#modalBody",
					model : book
				});
				bookModalView.render();
			});

			$('#btnAdd').click(function() {
				book = new Book({
					title : $('#title').val(),
					author : $('#author').val(),
					publisher : $('#publisher').val(),
					edition : $('#edition').val(),
					isbn : $('#isbn').val(),
					price : $('#price').val(),
					picture : $('#picture').val(),
					description : $('#description').val()
				});
				book.save({}, {
					type : 'POST',
					success : function() {
						$('#table_id').DataTable().destroy();
						booksTableView.render();
					},
					error : function() {
						alert("error with creating new book")
					}
				});
			});

			$('#btnSave').click(function() {
				book = new Book({
					bookId : $('#bookId').val(),
					title : $('#title').val(),
					author : $('#author').val(),
					publisher : $('#publisher').val(),
					edition : $('#edition').val(),
					isbn : $('#isbn').val(),
					price : $('#price').val(),
					picture : $('#picture').val(),
					description : $('#description').val()
				});
				book.save({}, {
					success : function() {
						$('#table_id').DataTable().destroy();
						booksTableView.render();
					},
					error : function() {
						alert("error with updating book")
					}
				});
			});

			$('#btnDelete').click(function() {
				book = new Book({
					id : $('#bookId').val().trim()
				});
				book.destroy({
					success : function() {
						$('#table_id').DataTable().destroy();
						booksTableView.render();
					},
					error : function() {
						alert("error with deleting book")
					}
				});
			});

		});