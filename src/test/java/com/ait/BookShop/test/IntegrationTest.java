package com.ait.BookShop.test;

import static org.junit.Assert.assertEquals;

import java.util.List;

import javax.ejb.EJB;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.ait.BookShop.data.BookDAO;
import com.ait.BookShop.model.Book;
import com.ait.BookShop.rest.BookWS;
import com.ait.BookShop.rest.JaxRsActivator;
import com.ait.BookShop.test.util.UtilsDAO;

@RunWith(Arquillian.class)
public class IntegrationTest {

	@Deployment
	public static Archive<?> createTestArchive() {
		return ShrinkWrap
				.create(JavaArchive.class, "Test.jar")
				.addClasses(BookDAO.class, Book.class, JaxRsActivator.class,
						BookWS.class, UtilsDAO.class)
				.addAsManifestResource("META-INF/persistence.xml",
						"persistence.xml")
				.addAsManifestResource(EmptyAsset.INSTANCE, "beans.xml");

	}

	@EJB
	private BookWS bookWS;

	@EJB
	private BookDAO bookDAO;

	@EJB
	private UtilsDAO utilsDAO;

	@Before
	public void setUp() {

		utilsDAO.deleteTable();
		Book book = new Book();
		book.setTitle("Object Oriented programming");
		book.setAuthor("Bala Swamy");
		book.setPublisher("Swamy papers");
		book.setEdition("first");
		book.setIsbn(15539269);
		book.setDescription("This book is all about object oriented programming.");
		book.setPrice("23.41");
		book.setPicture("logo-book.png");
		bookDAO.save(book);
		book = new Book();
		book.setTitle("Agile Testing");
		book.setAuthor("John Brown");
		book.setPublisher("MacGraw");
		book.setEdition("first");
		book.setIsbn(15532469);
		book.setDescription("This book is all about Agile testing.");
		book.setPrice("19.34");
		book.setPicture("back book.jpg");
		bookDAO.save(book);
	}

	@Test
	public void testGetAllBooks() {
		List<Book> bookList = bookDAO.findAllBooks();
		assertEquals("Data fetch = data persisted", bookList.size(), 2);
	}

	@Test
	public void testGetBookById() {
		Book book = bookDAO.findBookByBookId(1);
		assertEquals(book.getBookId(), 1);
		assertEquals(book.getTitle(), "Object Oriented programming");
		assertEquals(book.getAuthor(), "Bala Swamy");
		assertEquals(book.getPublisher(), "Swamy papers");
		assertEquals(book.getEdition(), "first");
		assertEquals(book.getIsbn(), 15539269);
		assertEquals(book.getDescription(),
				"This book is all about object oriented programming.");
		assertEquals(book.getPrice(), "23.41");
		assertEquals(book.getPicture(),"logo-book.png");
		book = bookDAO.findBookByBookId(2);
		assertEquals(book.getBookId(), 2);
		assertEquals(book.getTitle(), "Agile Testing");
		assertEquals(book.getAuthor(), "John Brown");
		assertEquals(book.getPublisher(), "MacGraw");
		assertEquals(book.getEdition(), "first");
		assertEquals(book.getIsbn(), 15532469);
		assertEquals(book.getDescription(),
				"This book is all about Agile testing.");
		assertEquals(book.getPrice(), "19.34");
		assertEquals(book.getPicture(),"back book.jpg");
	}

	@Test
	public void testGetBookByTitle() {
		List<Book> bookList = bookDAO.findBooksByTitle("Agile Testing");
		assertEquals(bookList.size(), 1);
		Book book = bookList.get(0);
		assertEquals("Agile Testing", book.getTitle());
	}

	@Test
	public void testGetBookByAuthor() {
		List<Book> bookList = bookDAO.findBooksByAuthor("John Brown");
		assertEquals(bookList.size(), 1);
		Book book = bookList.get(0);
		assertEquals("John Brown", book.getAuthor());
	}

	@Test
	public void testGetBookByPrice() {
		List<Book> bookList = bookDAO.findBooksByPrice("19.34");
		assertEquals(bookList.size(), 1);
		Book book = bookList.get(0);
		assertEquals("19.34", book.getPrice());
	}

	@Test
	public void testSaveBook() {
		Book book = new Book();
		book.setTitle("Applied Mathematics");
		book.setAuthor("RD Sharma");
		book.setPublisher("Sharma publications");
		book.setEdition("Ninth");
		book.setIsbn(1587249);
		book.setDescription("All about mathematics");
		book.setPrice("12.36");
		bookDAO.save(book);
		List<Book> bookList = bookDAO.findAllBooks();
		assertEquals(book.getBookId(), 3);
		assertEquals(book.getTitle(), "Applied Mathematics");
		assertEquals(book.getAuthor(), "RD Sharma");
		assertEquals(book.getPublisher(), "Sharma publications");
		assertEquals(book.getEdition(), "Ninth");
		assertEquals(book.getIsbn(), 1587249);
		assertEquals(book.getDescription(), "All about mathematics");
		assertEquals(book.getPrice(), "12.36");
	}

	@Test
	public void testUpdateBook() {
		Book book = bookDAO.findBookByBookId(2);
		book.setAuthor("Kelly Royer");
		book.setDescription("Agile principles and testing methods");
		book.setIsbn(42574374);
		bookDAO.update(book);
		bookDAO.findBookByBookId(2);
		assertEquals(book.getTitle(), "Agile Testing");
		assertEquals(book.getAuthor(), "Kelly Royer");
		assertEquals(book.getPublisher(), "MacGraw");
		assertEquals(book.getEdition(), "first");
		assertEquals(book.getIsbn(), 42574374);
		assertEquals(book.getDescription(),
				"Agile principles and testing methods");
		assertEquals(book.getPrice(), "19.34");
	}

	@Test
	public void testDeleteBook() {
		List<Book> bookList = bookDAO.findAllBooks();
		assertEquals(bookList.size(), 2);
		bookDAO.delete(1);
		bookList = bookDAO.findAllBooks();
		assertEquals(bookList.size(), 1);
		assertEquals(null, bookDAO.findBookByBookId(1));
	}
}
