package com.ait.BookShop.test;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import com.ait.BookShop.model.Book;

public class TestBookShop {

	private Book Book;

	@Before
	public void setup() {
		Book = new Book();
	}

	@Test
	public void testSetAndGetBookID() {
		Book.setBookId(1);
		assertEquals(1, Book.getBookId());
	}

	@Test
	public void testSetAndGetTitle() {
		Book.setTitle("Java Programming Basics");
		assertEquals("Java Programming Basics", Book.getTitle());
	}

	@Test
	public void testSetAndGetAuthor() {
		Book.setAuthor("John Brown");
		assertEquals("John Brown", Book.getAuthor());
	}

	@Test
	public void testSetAndGetIsbn() {
		Book.setIsbn(1268735395);
		assertEquals(1268735395, Book.getIsbn());
	}

	@Test
	public void testSetAndGetPublisher() {
		Book.setPublisher("Mountain Pages");
		assertEquals("Mountain Pages", Book.getPublisher());
	}

	@Test
	public void testSetAndGetEdition() {
		Book.setEdition("Second");
		assertEquals("Second", Book.getEdition());
	}

	@Test
	public void testSetAndGetDescription() {
		Book.setDescription("This book gives you good basic knowlege aboout Java.");
		assertEquals("This book gives you good basic knowlege aboout Java.",
				Book.getDescription());
	}

	@Test
	public void testSetAndGetPrice() {
		Book.setPrice("12.45");
		assertEquals("12.45", Book.getPrice());
	}

	@Test
	public void testSetAndGetPicture() {
		Book.setPicture("logo-book.png");
		assertEquals("logo-book.png", Book.getPicture());
	}

}
