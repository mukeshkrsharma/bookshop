package com.ait.BookShop.test;

import static org.junit.Assert.assertEquals;

import java.util.List;

import javax.ejb.EJB;
import javax.ws.rs.core.Response;

import org.apache.http.HttpStatus;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.ait.BookShop.data.BookDAO;
import com.ait.BookShop.model.Book;
import com.ait.BookShop.rest.BookWS;
import com.ait.BookShop.rest.JaxRsActivator;
import com.ait.BookShop.test.util.UtilsDAO;

@RunWith(Arquillian.class)
public class WSTest {
	@Deployment
	public static Archive<?> createTestArchive() {
		return ShrinkWrap
				.create(JavaArchive.class, "Test.jar")
				.addClasses(BookDAO.class, Book.class, JaxRsActivator.class,
						BookWS.class, UtilsDAO.class)
				// .addPackage(EventCause.class.getPackage())
				// .addPackage(EventCauseDAO.class.getPackage())
				// this line will pick up the production db
				.addAsManifestResource("META-INF/persistence.xml",
						"persistence.xml")
				.addAsManifestResource(EmptyAsset.INSTANCE, "beans.xml");

	}

	@EJB
	private BookWS bookWS;

	@EJB
	private BookDAO bookDAO;

	@EJB
	private UtilsDAO utilsDAO;

	@Before
	public void setUp() {

		utilsDAO.deleteTable();
		Book book = new Book();
		book.setTitle("Object Oriented programming");
		book.setAuthor("Bala Swamy");
		book.setPublisher("Swamy papers");
		book.setEdition("first");
		book.setIsbn(15539269);
		book.setDescription("This book is all about object oriented programming.");
		book.setPrice("23.41");
		book.setPicture("logo-book.png");
		bookDAO.save(book);
		book = new Book();
		book.setTitle("Agile Testing");
		book.setAuthor("John Brown");
		book.setPublisher("MacGraw");
		book.setEdition("first");
		book.setIsbn(15532469);
		book.setDescription("This book is all about Agile testing.");
		book.setPrice("19.34");
		book.setPicture("back book.jpg");
		bookDAO.save(book);
	}

	@Test
	public void testGetAllBooksWS() {
		Response response = bookWS.getAllBooks();
		List<Book> bookList = (List<Book>) response.getEntity();
		assertEquals(HttpStatus.SC_OK, response.getStatus());
		assertEquals("Data fetch = data persisted", bookList.size(), 2);
		Book book = bookList.get(0);
		assertEquals("Object Oriented programming", book.getTitle());
		book = bookList.get(1);
		assertEquals("Agile Testing", book.getTitle());
	}

	@Test
	public void testBookById() {
		Response response = bookWS.getBookByBookId(1);
		Book book = (Book) response.getEntity();
		assertEquals(book.getBookId(), 1);
		assertEquals(book.getTitle(), "Object Oriented programming");
		assertEquals(book.getAuthor(), "Bala Swamy");
		assertEquals(book.getPublisher(), "Swamy papers");
		assertEquals(book.getEdition(), "first");
		assertEquals(book.getIsbn(), 15539269);
		assertEquals(book.getDescription(),
				"This book is all about object oriented programming.");
		assertEquals(book.getPrice(), "23.41");
	}

	@Test
	public void testSaveBook() {
		Book book = new Book();
		book.setTitle("Applied Mathematics");
		book.setAuthor("RD Sharma");
		book.setPublisher("Sharma publications");
		book.setEdition("Ninth");
		book.setIsbn(1587249);
		book.setDescription("All about mathematics");
		book.setPrice("12.36");
		Response response = bookWS.saveBook(book);
		assertEquals(HttpStatus.SC_CREATED, response.getStatus());
		book = (Book) response.getEntity();
		assertEquals(book.getBookId(), 3);
		assertEquals(book.getTitle(), "Applied Mathematics");
		assertEquals(book.getAuthor(), "RD Sharma");
		assertEquals(book.getPublisher(), "Sharma publications");
		assertEquals(book.getEdition(), "Ninth");
		assertEquals(book.getIsbn(), 1587249);
		assertEquals(book.getDescription(), "All about mathematics");
		assertEquals(book.getPrice(), "12.36");
	}

	@Test
	public void testDeleteBook() {
		Response response = bookWS.getAllBooks();
		List<Book> bookList = (List<Book>) response.getEntity();
		assertEquals(bookList.size(), 2);
		bookWS.deleteBook(1);
		assertEquals(HttpStatus.SC_OK, response.getStatus());
		response = bookWS.getAllBooks();
		bookList = (List<Book>) response.getEntity();
		assertEquals(bookList.size(), 1);
		response = bookWS.getBookByBookId(1);
		Book book = (Book) response.getEntity();
		assertEquals(null, book);

	}

	@Test
	public void testUpdatebook() {
		Response response = bookWS.getBookByBookId(2);
		Book book = (Book) response.getEntity();
		book.setAuthor("Kelly Royer");
		book.setDescription("Agile principles and testing methods");
		book.setIsbn(42574374);
		response = bookWS.updateBook(book);
		assertEquals(HttpStatus.SC_OK, response.getStatus());
		book = (Book) response.getEntity();
		assertEquals(book.getTitle(), "Agile Testing");
		assertEquals(book.getAuthor(), "Kelly Royer");
		assertEquals(book.getPublisher(), "MacGraw");
		assertEquals(book.getEdition(), "first");
		assertEquals(book.getIsbn(), 42574374);
		assertEquals(book.getDescription(),
				"Agile principles and testing methods");
		assertEquals(book.getPrice(), "19.34");
	}

	@Test
	public void testGetBookByTitle() {
		Response response = bookWS.getByTitle("Agile Testing");
		List<Book> bookList = (List<Book>) response.getEntity();
		assertEquals(HttpStatus.SC_OK, response.getStatus());
		assertEquals(bookList.size(), 1);
		Book book = bookList.get(0);
		assertEquals("Agile Testing", book.getTitle());
	}

	@Test
	public void testGetBookByAuthor() {
		Response response = bookWS.getBooksByAuthor("John Brown");
		List<Book> bookList = (List<Book>) response.getEntity();
		assertEquals(HttpStatus.SC_OK, response.getStatus());
		assertEquals(bookList.size(), 1);
		Book book = bookList.get(0);
		assertEquals("John Brown", book.getAuthor());
	}

	@Test
	public void testGetBookByPrice() {
		Response response = bookWS.getBooksByPrice("19.34");
		List<Book> bookList = (List<Book>) response.getEntity();
		assertEquals(HttpStatus.SC_OK, response.getStatus());
		assertEquals(bookList.size(), 1);
		Book book = bookList.get(0);
		assertEquals("19.34", book.getPrice());
	}
}
