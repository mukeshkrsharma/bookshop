package com.ait.BookShop.test.util;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;



@Stateless
@LocalBean
public class UtilsDAO {

    @PersistenceContext
    private EntityManager em;
    
    public void deleteTable(){
        em.createQuery("DELETE FROM Book").executeUpdate();
        em.createNativeQuery("ALTER TABLE book AUTO_INCREMENT=1")
        .executeUpdate();
        
    }
      
}
