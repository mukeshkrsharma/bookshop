package com.ait.BookShop.model;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@StaticMetamodel(Book.class)
public abstract class Book_ {

	public static volatile SingularAttribute<Book, String> author;
	public static volatile SingularAttribute<Book, String> price;
	public static volatile SingularAttribute<Book, Integer> isbn;
	public static volatile SingularAttribute<Book, String> publisher;
	public static volatile SingularAttribute<Book, String> edition;
	public static volatile SingularAttribute<Book, String> description;
	public static volatile SingularAttribute<Book, String> title;
	public static volatile SingularAttribute<Book, String> picture;
	public static volatile SingularAttribute<Book, Integer> bookId;

}

